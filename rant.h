#include "stdafx.h"
#include<string>
#include "include/curl/curl.h"
#include<list>

using std::string;
using std::stoi;
using std::list;

string buffer;

size_t writer(char *ptr, size_t size, size_t nmemb, void *userdata) { ((string*)userdata)->append((char*)ptr, size, nmemb); return size * nmemb; }

class rant {

	/*
		Must be set or all the functions wouldnt return anything.
	*/
public: void ID(int id) {

	curl_global_init(CURL_GLOBAL_ALL);

	CURL* cl = curl_easy_init();
	string url = "https://devrant.com/api/devrant/rants/" + std::to_string(id) + "?app=3";
	curl_easy_setopt(cl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(cl, CURLOPT_WRITEFUNCTION, writer);
	curl_easy_setopt(cl, CURLOPT_WRITEDATA, &buffer);
	curl_easy_perform(cl);
	curl_easy_cleanup(cl);

}

public: string getAuthor() {

	string username = buffer.substr(buffer.find("user_username")).substr(buffer.find(","));
	return username.substr(0, username.find(",") - 1);
}

public: string getAuthorBackgroundColor() {

	string background = buffer.substr(buffer.find("user_avatar") + 19);
	return background.substr(0, background.find(",") - 1);
}

public: string getAuthorImage() {

	string image = buffer.substr(buffer.find("user_avatar"));
	image = image.substr(image.find("\"i\":") + 5);
	return image.substr(0, image.find("}") - 1);
}

public: int getScore() {

	string score = buffer.substr(buffer.find("score")).substr(buffer.find(",") - 9);
	return stoi(score.substr(0, score.find(",")));
}

public: string getText() {

	string score = buffer.substr(buffer.find("text")).substr(buffer.find(",") - 9);
	return score.substr(0, score.find(",") - 1);

}

public: int getDate() {

	string date = buffer.substr(buffer.find("created_time")).substr(buffer.find(",") - 2);
	return stoi(date.substr(0, date.find(",")));
}


public: bool exists() {

	string exist = buffer.substr(buffer.find("success") + 9);
	exist.substr(0, exist.find("}"));
	bool convert = (exist.substr(0, exist.find("}")) == "true");
	return convert;
}

public: int tagsCount(){
	string tags = buffer.substr(buffer.find("tags"));
	tags = tags.substr(8, tags.find("],") - 9);
	int count = 0;

	for (int i = 0; i < tags.rfind(","); i) {

		if (i > 0) {
			count += 1;
		}
		i = tags.find(",");
		tags = tags.substr(i+1);
	}
	count += 1;
	return count;
}

public: list<string> getTags() {

	list<string> taglist;

	string tags = buffer.substr(buffer.find("tags"));
	tags = tags.substr(8, tags.find("],") - 9);
	int count = 0;

	for (int i = 0; i < tags.rfind(","); i) {

		string temp;
		
		if (i > 0) {
			count += 1;
			temp = tags.substr(1, tags.find(",") - 2);
		}
		else temp = tags.substr(0, tags.find(",") - 1);

		taglist.push_back(temp.c_str());
		i = tags.find(",");
		tags = tags.substr(i+1);
		
	}
	count += 1;
	return taglist;
}

};

/*
This part is releated to the comments of a rant
*/

class comments {

public: int count() {

	string commentcount = buffer.substr(buffer.find("num_comments") + 14);
	return stoi(commentcount.substr(0, commentcount.find(",")));
}

	//This is to get all the comment messages

public: list<string> getUsernames() {

	list<string> usernames;

	string usernamelist = buffer.substr(buffer.find("comments\":[") + 10);
	usernamelist = usernamelist.substr(0, usernamelist.find("}}],\"") + 3);

	int username = usernamelist.find("user_username");
	for (int i = 0; i < count(); i++) {
		string s = usernamelist.substr(usernamelist.find("user_username", username)+9);
		s = s.substr(7, s.find(",") - 8);
		usernames.push_back(s);
		username = usernamelist.find("user_username", username) + 26;
	}
	return usernames;

}
	//This is not the score of the comment but the total score of the user.s
public: list<string> getUserScores() {

	list<string> userscores;

	string scorelist = buffer.substr(buffer.find("comments\":[") + 10);
	scorelist = scorelist.substr(0, scorelist.find("}}],\"") + 3);

	int userscore = scorelist.find("user_score");
	for (int i = 0; i < count(); i++) {
		string s = scorelist.substr(scorelist.find("user_score", userscore));
		s = s.substr(12, s.find(",")-12);
		userscores.push_back(s);
		userscore = scorelist.find("user_score", userscore) + 26;
	}
	return userscores;
}

		//This is the score of the comment.
public: list<string> getScores() {

	list<string> scores;

	string scorelist = buffer.substr(buffer.find("comments\":[") + 10);
	scorelist = scorelist.substr(0, scorelist.find("}}],\"") + 3);

	int score = scorelist.find("\"score\"");
	for (int i = 0; i < count(); i++) {
		string s = scorelist.substr(scorelist.find("\"score\"", score));
		s = s.substr(8, s.find(",") - 8);
		scores.push_back(s);
		score = scorelist.find("\"score\"", score) + 26;
	}
	return scores;
}

public: list<string> getMessages() {

	list<string> comments;

	string commentslist = buffer.substr(buffer.find("comments\":[") + 10);
	commentslist = commentslist.substr(0, commentslist.find("}}],\"") + 3);

	int body = commentslist.find("body");
	for (int i = 0; i < count(); i++) {
		string s = commentslist.substr(commentslist.find("body", body));
		s = s.substr(7, s.find(",") - 8);
		comments.push_back(s);
		body = commentslist.find("body", body) + 26;
	}
	return comments;
}
		//The creation date of the comments (in UNIX timestamp)
public: list<string> getDates() {

	list<string> dates;

	string dateslist = buffer.substr(buffer.find("comments\":[") + 10);
	dateslist = dateslist.substr(0, dateslist.find("}}],\"") + 3);

	int date = dateslist.find("created_time");
	for (int i = 0; i < count(); i++) {
		string s = dateslist.substr(dateslist.find("created_time", date));
		s = s.substr(14, s.find(",") - 14);
		dates.push_back(s);
		date = dateslist.find("created_time", date) + 26;
	}
	return dates;
}
	
public: list<string> getBackgroundColors() {

	list<string> backgrounds;

	string backgroundslist = buffer.substr(buffer.find("comments\":[") + 10);
	backgroundslist = backgroundslist.substr(0, backgroundslist.find("}}],\"") + 3);

	int background = backgroundslist.find("\"b\"");
	for (int i = 0; i < count(); i++) {
		string s = backgroundslist.substr(backgroundslist.find("\"b\"", background));
		s = s.substr(5, s.find("\"")+6);
		backgrounds.push_back(s);
		background = backgroundslist.find("\"b\"", background) + 26;
	}
	return backgrounds;
}

public: list<string> getImages() {

	list<string> images;


	string imageslist = buffer.substr(buffer.find("comments\":[") + 10);
	imageslist = imageslist.substr(0, imageslist.find("}}],\"") + 3);

	int image = imageslist.find("\"b\"");
	for (int i = 0; i < count(); i++) {
		string s = imageslist.substr(imageslist.find("\"b\"", image));
		s = s.substr(14, s.find(".jpg")-10);
		if (s.substr(0, 1) == "i") {
			images.push_back(s.substr(4));
		}
		else {
			images.push_back("None");
		}
		image = imageslist.find("\"b\"", image) + 26;
	}
	return images;
}

};